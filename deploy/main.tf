# Our backend is defined here. Located in our s3 and locked with DynamoDB table as stated below.
terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-2-tfstate"
    key            = "terraform/remote-backend/recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-2-tf-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3"
    }

  }
}
# Providers
provider "aws" {
  region = "us-east-1"

}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {

}
